import React from 'react'
import Jumbotrons from './pages/Jumbotrons'
import Jumbotron_down from './pages/Jumbotron_down'
import Layout from './pages/Layout'
import Footer from './pages/Footer'
import Navbars from './pages/Navbars'
import Animation from './pages/Animation'
import ReactCSSTransitionGroup from 'react-addons-css-transition-group'
import "./pages/animation.css"

export default class App extends React.Component{
  render(){
    return(
      <div>
        <ReactCSSTransitionGroup
        transitionName='example'
        transitionAppear={true}
        transitionAppearTimeout={3200}
        >
        <div className="css-test">
          <Navbars />
          <Layout />
          <Jumbotrons />
          <Animation />
          <Jumbotron_down />
          <Footer />                
        </div>
      </ReactCSSTransitionGroup>
      
      </div>
      
     
    );
    
  }
}

