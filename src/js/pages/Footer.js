import React from 'react'

export default class Footer extends React.Component {

  render() {
    return (
      <div>
        <footer>
          <div class="row">
            <div class="col-lg-4">
              <p>Copyright &copy; JoyceHsu.net</p>
            </div>
          </div>
        </footer>
      </div>
    );
  }
}
