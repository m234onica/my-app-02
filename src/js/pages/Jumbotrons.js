import React from 'react';
import TextInput from './TextInput'
import { Jumbotron, Container } from 'reactstrap';
import PropTypes from 'prop-types';

const Jumbotrons = (props) => {
  return (
      <div class="row">
        <div class="col-lg-6">
          <Jumbotron >
              <p className="lead">
                <TextInput initText="Enter your massage:" />
                <hr class="my-4" />
                <p>發現bug吃一包</p>
                <p>沒發現bug再吃一包</p>
                <p>解了bug開心吃一包</p>
                <p>解不了bug怒吃一包</p>
              </p>
          </Jumbotron>
        </div>
        <div class="col-sm-6">
          <img src="./tayu.jpg" height="420" width="420" />
        </div>
      </div>
  );
};
export default Jumbotrons

Jumbotron.propTypes = {
  // Pass in a Component to override default element
  tag: PropTypes.oneOfType([PropTypes.func, PropTypes.string]),
  fluid: PropTypes.bool,
  className: PropTypes.string
};
// export default class Jumbotron extends React.Component {

//   render() {
//     return (
//       <div class="container">
        
//             <div class="jumbotron">         
              
//               {/* <p class="lead">
//                 <a class="btn btn-primary btn-lg" href="./App" role="button">閱讀全文</a>
//               </p> */}
            
          
//           </div>
//         </div>
//       </div>
//     );
//   }
// }
