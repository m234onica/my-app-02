import React from 'react'
import TextInput_down from './TextInput_down'
import PropTypes from 'prop-types';

class Jumbotron_down extends React.Component {

  render() {
    return (
      <div>
          <div class="jumbotron">
            <TextInput_down initText="Enter your massage:" />
            <hr class="my-4" />            
            {/* <p class="lead">
              <a class="btn btn-primary btn-lg" href="./App" role="button">閱讀全文</a>
            </p> */}
          </div>
        </div>
    );
  }
}
Jumbotron_down.propTypes = {
  // Pass in a Component to override default element
  tag: PropTypes.oneOfType([PropTypes.func, PropTypes.string]),
  fluid: PropTypes.bool,
  className: PropTypes.string
};
export default Jumbotron_down
