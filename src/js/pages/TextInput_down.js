import React from 'react'
import TextShow_down from './TextShow_down'
import PropTypes from 'prop-types'

type Props = {
  initText: string,
}


class TextInput_down extends React.Component{
  state:{
    text: string,
  }
  constructor(props: Props) {
    super(props)
    this.state = {
      text: '',
    }
  }
  handleChange = (e: Event) => {
    if (e.target instanceof HTMLInputElement) {
      this.setState({ text: e.target.value })
    }
  }

  render() {
    return (
      <div>        
        <TextShow_down text={this.state.text} />
        <input type="text"
          value={this.state.text}
          placeholder={this.props.initText}
          onChange={this.handleChange}
        />
      </div>
    );
  }
}

TextInput_down.propTypes={
  initText:PropTypes.string.isRequired
}

export default TextInput_down
