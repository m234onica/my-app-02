# React.js Note
<h2>Reactjs必備步驟：</h2>

1. 安裝npm(網路教學許多，不再贅述)

3. 初始化project，在project中新增package.json檔案
```
npm init 
```
3. 安裝所需套件(安裝**webpack@4.0**以下的版本較穩定不出錯)
```
npm install --save-dev webpack@1.13.2 webpack-dev-server@1.14.1" html-webpack-plugin
```
```
npm install --save-dev babel-core babel-eslint babel-loader babel-preset-es2015 babel-preset-react eslint eslint-config-airbnb eslint-loader eslint-plugin-import eslint-plugin-jsx-a11y eslint-plugin-react 
```



## 架構
```
Project: my-app-02

    |-- node_modules/
    |-- src/
        |-- js/
            |-- component/
                |-- Archives.js
                |-- Feature.js
                |-- Setting.js
            |-- pages
                |-- Footer.js
                |-- Jumbotron.js
                |-- Jumbotron_down.js
                |-- Style.css
                |-- ...
            |-- App.js
            |-- Client.js
        |-- client.min.js
        |-- index.html
    |-- package.json
    |-- package-lock.json
    |-- webpack.config.js
```

* 生成client.min.js檔:
```
npm install
webpack
```

* 安裝webpack-dev-server套件
```
npm install -g webpack-dev-server
```

* 執行project
```
webpack-dev-server --content-base src
```
>註：
>接著就能到`https://localhost:8080`或著`https://localhost:8080/webpack-dev-server/index.html`去查看

* 使用`npm run dev`快速執行Project
```
webpack-dev-server --content-base src --inline --hot
```

* package.json檔加入 "dev"指令
```
"scripts": {
    "dev": "./node_modules/.bin/webpack-dev-server -->content-base src --inline --hot",
    ...
  },
```

* 執行project
```
npm run dev
```

# Reactjs(Reactstrap) + navbar
* 安裝套件：
```
npm install --save reactstrap react react-dom
```

# Reactjs + CSS

* 安裝這兩個套件
```
npm install css-loader style-loader
```

main.js
```
import './main.css';
// Other code
```
main.css(Navbar的舉例樣式)
```
body {
  padding-top: 3.5rem;
}

/*
 * Sidebar
 */

.sidebar {
  position: fixed;
  top: 51px;
  bottom: 0;
  left: 0;
  z-index: 1000;
  padding: 20px;
  overflow-x: hidden;
  overflow-y: auto; /* Scrollable contents if viewport is shorter than content. */
  border-right: 1px solid #eee;
}

/* Sidebar navigation */
.sidebar {
  padding-left: 0;
  padding-right: 0;
}

.sidebar .nav {
  margin-bottom: 20px;
}

.sidebar .nav-item {
  width: 100%;
}

.sidebar .nav-item + .nav-item {
  margin-left: 0;
}

.sidebar .nav-link {
  border-radius: 0;
}


```

webpack.config.js
```
module: {
    loaders: [{
      test: /\.jsx$/,
      loader: 'babel'
    }, {
      test: /\.css$/, // Only .css files
      loader: 'style!css' // Run both loaders
    }]
  }
};
```
